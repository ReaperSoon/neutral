var app = {
    /* Properties */

    items: [],

    /* Methods */
    initialize: function() {
        this.bindEvents();
        this.bindUserEvents();
        app.getItems();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('resume', this.onResume);
    },
    bindUserEvents: function() {
    },
    onDeviceReady: function() {
        console.log("Device ready");
        /*  app.getItems(); */
    },
    onResume: function() {
        console.log('Resuming');
    },
    getItems: function() {
        console.log('Loading items to compare ...');

        var itemsToCompare = JSON.parse(localStorage.comparingItems);

        for (var i = 0; i < itemsToCompare.length; i++) {
            var item = itemsToCompare[i];
            app.items.push(item);
            app.addItemToList(item);
        }
    },
    addItemToList: function(item) {
        var injectionMap = {
            "*[inject=image]" : function(elem) { elem.attr('src', "img/phones/" + item.id + Settings.images.format); },
            "*[inject=smartphone_name]" : function(elem) { elem.html(item.name); },
            "*[inject=price]" : function(elem) { elem.html(item.price + Settings.currency.char); },            
            "*[inject=01_crit]" : function(elem) { elem.html(item.display.size); },
            "*[inject=02_crit]" : function(elem) { elem.html(item.display.resolution); },
            "*[inject=03_crit]" : function(elem) { elem.html(item.display.screen); },
            "*[inject=04_crit]" : function(elem) { elem.html(item.hardware.cpu); },
            "*[inject=05_crit]" : function(elem) { elem.html(item.hardware.cpu_architecture); },
            "*[inject=06_crit]" : function(elem) { elem.html(item.hardware.gpu); },
            "*[inject=07_crit]" : function(elem) { elem.html(item.hardware.ram); },
            "*[inject=08_crit]" : function(elem) { elem.html(item.camera.resolution); },
            "*[inject=09_crit]" : function(elem) { elem.html(item.camera.lens); },
            "*[inject=10_crit]" : function(elem) { elem.html(item.camera.flash); },
            "*[inject=11_crit]" : function(elem) { elem.html(item.camera.autofocus); },
            "*[inject=12_crit]" : function(elem) { elem.html(item.camera.image_stabilization); },
            "*[inject=13_crit]" : function(elem) { elem.html(item.camera.panorama); },
            "*[inject=14_crit]" : function(elem) { elem.html(item.camera.aperture); },
            "*[inject=15_crit]" : function(elem) { elem.html(item.camera.front_camera); },
            "*[inject=16_crit]" : function(elem) { elem.html(item.camera.video_resolution); },
            "*[inject=17_crit]" : function(elem) { elem.html(item.battery.type); },
            "*[inject=18_crit]" : function(elem) { elem.html(item.battery.capacity); },
            "*[inject=19_crit]" : function(elem) { elem.html(item.feature.headphone_slot); },
            "*[inject=20_crit]" : function(elem) { elem.html(item.feature.fm_radio); },
            "*[inject=21_crit]" : function(elem) { elem.html(item.connectivity.network_compatibility); },
            "*[inject=22_crit]" : function(elem) { elem.html(item.connectivity.wifi); },
            "*[inject=23_crit]" : function(elem) { elem.html(item.connectivity.nfc); },
            "*[inject=24_crit]" : function(elem) { elem.html(item.connectivity.bluetooth); },
            "*[inject=25_crit]" : function(elem) { elem.html(item.software.operating_system); },
            "*[inject=26_crit]" : function(elem) { elem.html(item.feature.internal_storage); },
            "*[inject=27_crit]" : function(elem) { elem.html(item.feature.card_format); },
            "*[inject=28_crit]" : function(elem) { elem.html(item.feature.sim_card); },
            "*[inject=29_crit]" : function(elem) { elem.html(item.other_info.phone_size); },
            "*[inject=30_crit]" : function(elem) { elem.html(item.other_info.phone_weight); },
            "*[inject=31_crit]" : function(elem) { elem.html(item.sensors.location); },
            "*[inject=32_crit]" : function(elem) { elem.html(item.sensors.gyroscope); },
            "*[inject=33_crit]" : function(elem) { elem.html(item.sensors.accelerometer); },
            "*[inject=34_crit]" : function(elem) { elem.html(item.sensors.light_sensor); },
            "*[inject=35_crit]" : function(elem) { elem.html(item.sensors.proximity_sensor); },
            "*[inject=36_crit]" : function(elem) { elem.html(item.sensors.compass); },
            "*[inject=37_crit]" : function(elem) { elem.html(item.sensors.thermometer); },
            "*[inject=38_crit]" : function(elem) { elem.html(item.sensors.barometer); },
            "*[inject=39_crit]" : function(elem) { elem.html(item.sensors.hygrometer); }
        };
        Util.appendModule('#compare > .content', 'modules/comparator-item.html', injectionMap, function() {
            console.log('item' + item.id + ' injected');
        });
    },
    openComparePage: function() {
        window.location = Settings.comparator.url;
    }
};

app.initialize();
