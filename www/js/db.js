var database = {
	phones: [
	{
		id: 1,
		name: "iPhone 5S",
		brand: "Apple",
		price: 509.00,
		display: {
			size: "4\"",
			resolution: "1136 x 640 (325 ppi)",
			screen: "IPS LCD"
		},
		hardware: {
			cpu: "1.3 GHz, ARM Cyclone (64 bit)",
			cpu_architecture: "Dual core",
			gpu: "300 MHz, PowerVR Series 6 Rogue G6430",
			ram: "1 GB"
		},
		camera: {
			resolution: "8 MP (3264 x 2448)",
			lens: "f/2.2, 4.3 mm",
			flash: "Yes, Dual LED",
			autofocus: "Yes",
			image_stabilization: "Digital",
			panorama: "Yes",
			aperture: "f/2.2",
			front_camera: "1.23 MP",
			video_resolution: "1920 x 1080 @ 30fps"
		},
		battery: {
			type: "Lithium-Ion Polymer (Non-removable)",
			capacity: "1570 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "No",
			internal_storage: "16BG, 32GB, 64GB",
			card_format: "None",
			sim_card: "Nano-SIM"
		},
		connectivity: {
			network_compatibility: "4G, LTE (100Mb/s)",
			wifi: "802.11a/b/g/n (Hotspot)",
			nfc: "No",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "iOS 7"
		},
		sensors: {
			location: "A-GPS, GLONASS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "No",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "58.6 x 123.8 x 7.6 mm",
			phone_weight: "112 g"
		}
	},
	{
		id: 2,
		name: "iPhone 6S Plus",
		brand: "Apple",
		price: 749.00,
		display: {
			size: "5.5\"",
			resolution: "1920 x 1080 (400 ppi)",
			screen: "Super IPS LCD2"
		},
		hardware: {
			cpu: "1.84 GHz, Apple A9 APL1022",
			cpu_architecture: "Dual core",
			gpu: "PowerVR GT7600",
			ram: "2 GB"
		},
		camera: {
			resolution: "12.6 MP (4096 x 3072)",
			lens: "f/2.2, 29 mm",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "Optical",
			panorama: "No",
			aperture: "f/2.2",
			front_camera: "5 MP",
			video_resolution: "3840 x 2160 @ 30 fps"
		},
		battery: {
			type: "Lithium-Ion (Non-removable)",
			capacity: "2750 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "No",
			internal_storage: "128 GB",
			card_format: "None",
			sim_card: "Nano-SIM"
		},
		connectivity: {
			network_compatibility: "4G, LTE (100 Mb/s)",
			wifi: "802.11a/b/g/n (Hotspot)",
			nfc: "Yes",
			bluetooth: "No"
		},
		software : {
			operating_system: "iOS 9"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "Yes",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "77.9 x 158.2 x 7.3 mm",
			phone_weight: "192 g"
		}
	},
	{
		id: 3,
		name: "Galaxy A5",
		brand: "Samsung",
		price: 349.99, 
		display: {
			size: "5.2\"",
			resolution: "1920 x 1080 (423 ppi)",
			screen: "AMOLED"
		},
		hardware: {
			cpu: "1.6 GHz",
			cpu_architecture: "-",
			gpu: "-",
			ram: "2 GB"
		},
		camera: {
			resolution: "13.6 MP (4263 x 3197)",
			lens: "f/1.9",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "Optical",
			panorama: "No",
			aperture: "f/1.9",
			front_camera: "5 MP",
			video_resolution: "-"
		},
		battery: {
			type: "Lithium-Ion (Removable)",
			capacity: "2900 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "No",
			internal_storage: "16 GB",
			card_format: "SDXC",
			sim_card: "Micro-SIM"
		},
		connectivity: {
			network_compatibility: "3G, UMTS (0.38Mb/s)",
			wifi: "802.11a/b/g/n",
			nfc: "Yes",
			bluetooth: "Yes"
		},
		software : {
			operating_system: "Android"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "No",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "No",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "71 x 144.8 x 7.3 mm",
			phone_weight: "155 g"
		}
	},
	{
		id: 4,
		name: "Galaxy S6",
		brand: "Samsung",
		price: 599.00,
		display: {
			size: "5.1\"",
			resolution: "2560 x 1440 (575 ppi)",
			screen: "AMOLED"
		},
		hardware: {
			cpu: "2.1 GHz, Exynos 7 Octa 7420",
			cpu_architecture: "Octa core",
			gpu: "Mali-T760 MP8",
			ram: "3 GB"
		},
		camera: {
			resolution: "16.8 MP (4729 x 3547)",
			lens: "f/1.9, 28 mm",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "Optical",
			panorama: "Yes",
			aperture: "f/1.9",
			front_camera: "5 MP",
			video_resolution: "3840 x 2160 @ 30 fps"
		},
		battery: {
			type: "Lithium-Ion (Non-removable)",
			capacity: "2550 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "No",
			internal_storage: "128 GB",
			card_format: "-",
			sim_card: "Nano-SIM"
		},
		connectivity: {
			network_compatibility: "4G, LTE (100 Mb/s)",
			wifi: "802.11a/b/g/n (Hotspot)",
			nfc: "Yes",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 5.1"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "Yes",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "70.5 x 143.4 x 6.8 mm",
			phone_weight: "138 g"
		}
	},
	{
		id: 5,
		name: "Xperia Z3",
		brand: "Sony",
		price: 549.00,
		display: {
			size: "5.2\"",
			resolution: "1920 x 1080 (423 ppi)",
			screen: "Super IPS LCD2"
		},
		hardware: {
			cpu: "2.5 GHz Qualcomm Snapdragon 801 8974-AC",
			cpu_architecture: "Quad core",
			gpu: "Adreno 330",
			ram: "3 GB"
		},
		camera: {
			resolution: "21.7 MP (5379 x 4034)",
			lens: "f/2.0, 25 mm",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "Digital",
			panorama: "Yes",
			aperture: "f/2.0",
			front_camera: "2.2 MP",
			video_resolution: "3840 x 2160 @ 30 fps"
		},
		battery: {
			type: "Lithium-Ion (Non-removable)",
			capacity: "3100 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "Yes",
			internal_storage: "16 GB",
			card_format: "SDXC",
			sim_card: "Nano-SIM"
		},
		connectivity: {
			network_compatibility: "3G, UMTS (0.38 Mb/s)",
			wifi: "802.11a/b/g/n (Hotspot)",
			nfc: "Yes",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 5.0"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "Yes",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "72 x 146 x 7.3 mm",
			phone_weight: "152 g"
		}
	},
	{
		id: 6,
		name: "Xperia Z5 Premium",
		brand: "Sony",
		price: 789.00,
		display: {
			size: "5.5\"",
			resolution: "3840 x 2160 (801 ppi)",
			screen: "Super IPS LCD2"
		},
		hardware: {
			cpu: "2 GHz, Qualcomm Snapdragon 810 MSM8994",
			cpu_architecture: "Octa core",
			gpu: "Adreno 430",
			ram: "3 GB"
		},
		camera: {
			resolution: "24.1 MP (5670 x 4252)",
			lens: "-",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "Digital",
			panorama: "No",
			aperture: "-",
			front_camera: "5 MP",
			video_resolution: "3840 x 2160"
		},
		battery: {
			type: "Lithium-Ion (Non-removable)",
			capacity: "3430 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "Yes",
			internal_storage: "32 GB",
			card_format: "SDXC",
			sim_card: "Nano-SIM"
		},
		connectivity: {
			network_compatibility: "3G UMTS (0.38 Mb/s)",
			wifi: "802.11a/b/g/n (Hotspot)",
			nfc: "Yes",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 5.1"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "Yes",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "76 x 154.4 x 7.8 mm",
			phone_weight: "180 g"
		}
	},
	{
		id: 7,
		name: "One",
		brand: "One Plus",
		price: 299.00,
		display: {
			size: "5.5\"",
			resolution: "1920 x 1080 (400 ppi)",
			screen: "Super IPS LCD2"
		},
		hardware: {
			cpu: "2.5 GHz, Qualcomm Snapdragon 801 8974-AC",
			cpu_architecture: "Quad core",
			gpu: "Adreno 330",
			ram: "3 GB"
		},
		camera: {
			resolution: "13.6 MP (4263 x 3197)",
			lens: "f/2.0",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "Digital",
			panorama: "Yes",
			aperture: "f/2.0",
			front_camera: "5 MP",
			video_resolution: "3840 x 2160 @ 120 fps"
		},
		battery: {
			type: "Lithium-Ion (Removable)",
			capacity: "3100 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "Yes",
			internal_storage: "16 GB, 64 GB",
			card_format: "SDXC",
			sim_card: "Micro-SIM"
		},
		connectivity: {
			network_compatibility: "4G, LTE (100 Mb/s)",
			wifi: "802.11a/b/g/n (Hotspot)",
			nfc: "yes",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 5.1"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "No",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "75.9 x 152.9 x 8.9 mm",
			phone_weight: "162 g"
		}
	},
	{
		id: 8,
		name: "X",
		brand: "One Plus",
		price: 309.99,
		display: {
			size: "5\"",
			resolution: "1920 x 1080 (440 ppi)",
			screen: "AMOLED"
		},
		hardware: {
			cpu: "2.3 GHz, Qualcomm Snapdragon 801",
			cpu_architecture: "Quad core",
			gpu: "Adreno 330",
			ram: "3 GB"
		},
		camera: {
			resolution: "13.6 MP (4263 x 3197)",
			lens: "f/2.2",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "Digital",
			panorama: "Yes",
			aperture: "f/2.2",
			front_camera: "8 MP",
			video_resolution: "1920 x 1080 @ 120 fps"
		},
		battery: {
			type: "Lithium-Ion Polymer (Non-removable)",
			capacity: "2525 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "Yes",
			internal_storage: "16 GB",
			card_format: "SDXC",
			sim_card: "Nano-SIM"
		},
		connectivity: {
			network_compatibility: "4G, LTE (100 Mb/s)",
			wifi: "802.11b/g/n",
			nfc: "No",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 5.1"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "No",
			thermometer: "No",
			barometer: "No",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "69 x 140 x 6.9 mm",
			phone_weight: "138 g"
		}
	},
	{
		id: 9,
		name: "P8max",
		brand: "Huawei",
		price: 549.00,
		display: {
			size: "6.8\"",
			resolution: "1920 x 1080 (323 ppi)",
			screen: "-"
		},
		hardware: {
			cpu: "2 Ghz, HiSilicon Kirin 935",
			cpu_architecture: "Octa core",
			gpu: "Mali-T628 MP4",
			ram: "3 GB"
		},
		camera: {
			resolution: "13.6 MP (4263 x 3197)",
			lens: "f/2.0",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "Optical",
			panorama: "No",
			aperture: "f/2.0",
			front_camera: "5 MP",
			video_resolution: "1920 x 1080"
		},
		battery: {
			type: "Lithium-Ion (Non-removable)",
			capacity: "4360 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "No",
			internal_storage: "64 GB",
			card_format: "SDXC",
			sim_card: "Micro-SIM"
		},
		connectivity: {
			network_compatibility: "4G, LTE (100 Mb/s)",
			wifi: "802.11a/b/g/n",
			nfc: "Yes",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 5.0"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "No",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "93 x 182.7 x 6.8 mm",
			phone_weight: "228 g"
		}
	},
	{
		id: 10,
		name: "Nexus 6P",
		brand: "Google",
		price: 599.00,
		display: {
			size: "5.7\"",
			resolution: "2560 x 1440 (515 ppi)",
			screen: "AMOLED"
		},
		hardware: {
			cpu: "2 GHz, Qualcomm Snapdragon 810 MSM8994",
			cpu_architecture: "Octa core",
			gpu: "Adreno 430",
			ram: "3 GB"
		},
		camera: {
			resolution: "12.9 MP (4146 x 3110)",
			lens: "f/2.0",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "None",
			panorama: "No",
			aperture: "f/2.0",
			front_camera: "8 MP",
			video_resolution: "3840 x 2160 @ 30 fps"
		},
		battery: {
			type: "Lithium-Ion (Non-removable)",
			capacity: "3450 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "No",
			internal_storage: "128 g",
			card_format: "SDXC",
			sim_card: "Micro-SIM"
		},
		connectivity: {
			network_compatibility: "4G LTE, (100 Mb/s)",
			wifi: "802.11a/b/g/n",
			nfc: "Yes",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 6.0"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "Yes",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "77.8 x 159.3 x 7.3 mm",
			phone_weight: "178 g"
		}
	},
	{
		id: 11,
		name: "One X",
		brand: "HTC",
		price: 259.00,
		display: {
			size: "4.7\"",
			resolution: "1280 x 720 (312 ppi)",
			screen: "Super IPS LCD2"
		},
		hardware: {
			cpu: "1.5 GHz, ARM Cortex A9",
			cpu_architecture: "Quad core",
			gpu: "520 MHz, Nvidia ULP GeForce",
			ram: "1 GB"
		},
		camera: {
			resolution: "8 MP (3264 x 2448)",
			lens: "f/2.0, 4mm",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "None",
			panorama: "Yes",
			aperture: "f/2.0",
			front_camera: "1.3 MP",
			video_resolution: "1920 x 1080 @ 24 fps"
		},
		battery: {
			type: "Lithium-Ion Polymer (Non-removable)",
			capacity: "1800 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "Yes",
			internal_storage: "16GB, 32 GB",
			card_format: "None",
			sim_card: "Micro-SIM"
		},
		connectivity: {
			network_compatibility: "4G, HSPA+ (84 Mb/s)",
			wifi: "802.11a/b/g/n (Hotspot)",
			nfc: "Yes",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 4.2.2"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "No",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "69.9 x 134.8 x 8.9 mm",
			phone_weight: "130 g"
		}
	},
	{
		id: 12,
		name: "One M9",
		brand: "HTC",
		price: 454.99,
		display: {
			size: "5\"",
			resolution: "1920 x 1080 (440 ppi)",
			screen: "Super IPS LCD2"
		},
		hardware: {
			cpu: "2 GHz, Qualcomm Snapdragon 810 MSM8994",
			cpu_architecture: "Octa core",
			gpu: "Adreno 430",
			ram: "3 GB"
		},
		camera: {
			resolution: "21 MP (5287 x 3965)",
			lens: "f/2.2 27.8 mm",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "Digital",
			panorama: "Yes",
			aperture: "f/2.2",
			front_camera: "4 MP",
			video_resolution: "3840 x 2160 @ 30 fps"
		},
		battery: {
			type: "Lithium-Ion Polymer (Non-removable)",
			capacity: "2840 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "Yes",
			internal_storage: "32 GB",
			card_format: "SDXC",
			sim_card: "Nano-SIM"
		},
		connectivity: {
			network_compatibility: "4G, LTE (100 Mb/s)",
			wifi: "802.11a/b/g/n (Hotspot)",
			nfc: "Yes",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 5.1"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "No",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "69.7 x 144.6 x 9.6 mm",
			phone_weight: "157 g"
		}
	},
	{
		id: 13,
		name: "K4 Note",
		brand: "Lenovo",
		price: 21.0,
		display: {
			size: "5.5\"",
			resolution: "1920 x 1080 (400 ppi)",
			screen: "Super IPS LCD2"
		},
		hardware: {
			cpu: "1.5 GHz, MediaTek MT6753",
			cpu_architecture: "-",
			gpu: "ARM Mali-T720 MP3",
			ram: "3 GB"
		},
		camera: {
			resolution: "13.6 MP (4263 x 3197)",
			lens: "-",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "None",
			panorama: "No",
			aperture: "-",
			front_camera: "5 MP",
			video_resolution: "-"
		},
		battery: {
			type: "Lithium-Ion (Non-removable)",
			capacity: "3300 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "Yes",
			internal_storage: "16 GB",
			card_format: "SDXC",
			sim_card: "Micro-SIM"
		},
		connectivity: {
			network_compatibility: "4G, LTE (100 Mb/s)",
			wifi: "802.11a/b/g/n (Hotspot)",
			nfc: "Yes",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 5.1"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "No",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "No",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "76.5 x 153.7 x 9.1 mm",
			phone_weight: "160 g"
		}
	},
		{
		id: 14,
		name: "Galaxy S7 Edge",
		brand: "Samsung",
		price: 799.0,
		display: {
			size: "5.5\"",
			resolution: "2560 x 1440 (534 ppi)",
			screen: "AMOLED"
		},
		hardware: {
			cpu: "2.3 GHz, Exynos 8 Octa 8890",
			cpu_architecture: "-",
			gpu: "ARM Mali-T880 MP14",
			ram: "4 GB"
		},
		camera: {
			resolution: "12.6 MP (4096 x 3072)",
			lens: "f/1.7",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "Optical",
			panorama: "No",
			aperture: "f/1.7",
			front_camera: "5 MP",
			video_resolution: "3840 x 2160"
		},
		battery: {
			type: "Lithium-Ion (Non-removable)",
			capacity: "3600 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "No",
			internal_storage: "64 GB",
			card_format: "SDXC",
			sim_card: "Nano-SIM"
		},
		connectivity: {
			network_compatibility: "4G, LTE (100 Mb/s)",
			wifi: "802.11a/b/g/n (Hotspot)",
			nfc: "Yes",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 6.0"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "Yes",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "72.6 x 150.9 x 7.7 mm",
			phone_weight: "157 g"
		}
	},
	{
		id: 15,
		name: "Mi 5",
		brand: "Xiaomi",
		price: 489.99,
		display: {
			size: "5.2\"",
			resolution: "1920 x 1080 (423 ppi)",
			screen: "Super IPS LCD2"
		},
		hardware: {
			cpu: "Qualcomm Snapdragon 820 MSM8996",
			cpu_architecture: "Quad core",
			gpu: "Adreno 530",
			ram: "4 GB"
		},
		camera: {
			resolution: "16.8 MP (4729 x 3547)",
			lens: "f/2.0",
			flash: "Yes, LED",
			autofocus: "Yes",
			image_stabilization: "Optical",
			panorama: "No",
			aperture: "f/2.0",
			front_camera: "4 MP",
			video_resolution: "3840 x 2160 @ 30 fps"
		},
		battery: {
			type: "Lithium-Ion (Non-removable)",
			capacity: "3000 mAh"
		},
		feature: {
			headphone_slot: "3.5 mm",
			fm_radio: "No",
			internal_storage: "128 Gb",
			card_format: "SDXC",
			sim_card: "Nano-SIM"
		},
		connectivity: {
			network_compatibility: "4G, LTE (100 Mb/s)",
			wifi: "802.11a/b/g/n (Hotspot)",
			nfc: "Yes",
			bluetooth: "4.0"
		},
		software : {
			operating_system: "Android 6.0"
		},
		sensors: {
			location: "A-GPS",
			gyroscope: "Yes",
			accelerometer: "Yes",
			light_sensor: "Yes",
			proximity_sensor: "Yes",
			compass: "Yes",
			thermometer: "No",
			barometer: "Yes",
			hygrometer: "No"
		},
		other_info: {
			phone_size: "69.2 x 144.6 x 7.3 mm",
			phone_weight: "139 g"
		}
	}
	],
	getPhones: function(offset, limit) {
		offset = offset !== 'undefined' ? offset : 0;
		limit = limit !== 'undefined' ? limit : 0;
		var end = (offset+limit) <= this.phones.length ? (offset+limit) : this.phones.length;

		return this.phones.slice(offset, end);
	},
	getPhonesFiltered : function(filters) {

	}
};