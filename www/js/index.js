var app = {
    /* Properties */
    items: [],
    itemsToCompare: [],

    /* Methods */
    initialize: function() {
        this.bindEvents();
        this.bindUserEvents();
        this.initZones();
        app.getItems(app.items.length, Settings.home.first_items_load_nbr);
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('resume', this.onResume);
    },
    bindUserEvents: function() {
        $('#home .seemore').click(app.seeMoreHandler);
        $('#navigation-bar .filters').click(app.openFilters);
        $('#home #compare_bar .arrow_compare').click(app.openComparePage);
    },
    onDeviceReady: function() {
        console.log("Device ready");
        if (typeof StatusBar !== "undefined" && StatusBar != null) {
            StatusBar.hide();
        }
    },
    onResume: function() {
        console.log('Resuming');
    },
    onItemSelected: function() {
        var checked = $('#home .item input:checkbox:checked').length;
        if (checked === 4) {
            $('#home .item input:checkbox:not(:checked)').attr('disabled', true);
        }else if (checked > 1 && checked < 4) {
            app.openComparaisonBar();
            $('#home .item input:checkbox').attr('disabled', false);
        }else {
            app.closeComparaisonBar();
        }

        var itemId = $(this).attr('id').substr(1) - 1;
        var item = app.items[itemId];
        if (this.checked) {
            app.addItemToCompareBar(item);
            $('#elem' + (itemId+1)).addClass('selected');
        }else {
            app.removeItemFromCompareBar(item);
            $('#elem' + (itemId+1)).removeClass('selected');
        }
    },
    openComparaisonBar: function() {
        $('#home #compare_bar').addClass('show');
    },
    closeComparaisonBar: function() {
        $('#home #compare_bar').removeClass('show');
    },
    seeMoreHandler: function() {
        console.log("See more clicked");
        app.getItems(app.items.length, Settings.home.nbr_item_loading);
    },
    getItems: function(offset, limit) {
        console.log('Loading items from ' + offset + ' to ' + limit);
        var newItems = database.getPhones(offset, limit);
        $('#home .seemore').hide();
        $('#home .loading').show();
        for (var i = 0; i < newItems.length; i++) {
            var item = newItems[i];
            app.items.push(item);
            app.addItemToList(item);
        }
        $('#home .loading').hide();
        if (app.items.length < database.phones.length) {
            $('#home .seemore').show();
        }
    },
    addItemToList: function(item) {
        var injectionMap = {
            "*[inject=image]" : function(elem) {
                elem.attr('src', 'img/phones/' + item.id + Settings.images.format);
            },
            "*[inject=title]" : function(elem) {
                elem.html(item.brand + ' - ' + item.name);
            },
            "*[inject=checkbox]" : function(elem) {
                elem.attr('id', 'c' + item.id);
                elem.click(app.onItemSelected);
            },
            "*[inject=label]" : function(elem) {
                elem.attr('for', 'c' + item.id);
            },
            "*[inject=price]" : function(elem) {
                elem.html(item.price + Settings.currency.char);
            },
            "self" : function(elem) {
                elem.click(function() {
                    var checked = $('#home .item input:checkbox:checked').length;
                    if (checked < 4 || $(item).hasClass('selected') == false) {
                        $('#c' + item.id).trigger('click');
                    }
                });
                elem.attr('id', 'elem' + item.id);
            }
        };
        Util.appendModule('#home > .list', 'modules/home-item.html', injectionMap);
    },
    addItemToCompareBar: function(item) {
        var injectionMap = {
            "*[inject=image]" : function(elem) {
                elem.attr('src', 'img/phones/' + item.id + Settings.images.format);
            },
            "*[inject=name]" : function(elem) {
                elem.html(item.name);
            },
            "self" : function(elem) {
                elem.attr('id', 'compare-item-' + item.id);
                elem.draggable({ cursor: "crosshair", revert: "invalid"});
                elem.addClass('draggable');
            }
        };
        Util.appendModule('#compare_bar', 'modules/compare-bar-item.html', injectionMap);
        app.itemsToCompare.push(item);
    },
    removeItemFromCompareBar: function(item) {
        console.log('removing ' + 'compare-item-' + item.id);
        $('#compare-item-' + item.id).remove();
        app.itemsToCompare = app.itemsToCompare.filter(function (el) { return el.id !== item.id; });
        console.log(app.itemsToCompare);
    },
    openFilters: function() {
        $('#filters-panel').toggleClass("show");
    },
    openComparePage: function() {
        console.log("Open compare page");
        localStorage.comparingItems = JSON.stringify(app.itemsToCompare);
        window.location = Settings.comparator.url;
    },
    initZones: function() {
        $("#drop").droppable({ accept: ".draggable", 
            drop: function(event, ui) {
                var dropped = ui.draggable;
                var item = app.items[dropped.attr('id').substr(13) - 1];
                $(dropped).remove();
                $('#c' + item.id).trigger('click');
            }, 
            over: function(event, elem) {
                $(this).addClass("over");
                console.log("over");
            },
            out: function(event, elem) {
                $(this).removeClass("over");
            },
            activate: function(event, elem) {
                $(this).addClass('show');
            },
            deactivate: function(event, elem) {
                var droppedOn = $(this);
                droppedOn.removeClass("over").removeClass('show');
            }
        });
        $("#drop").sortable();
        $("#origin").droppable({ accept: ".draggable",
            drop: function(event, ui) {
                console.log("drop");
                $(this).removeClass("border").removeClass("over");
                var dropped = ui.draggable;
                var droppedOn = $(this);
                $(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);      
             }
         });
    }
};

/* Init sliders */
$(function() {
    $( "#slider-price" ).slider({
      range: true,
      min: 1,
      max: 1000,
      values: [ 100, 500 ],
      slide: function( event, ui ) {
        $( "#price-amount-label" ).val( $("<div>").html(Settings.currency.char).text() + ui.values[ 0 ] + " - " + $("<div>").html(Settings.currency.char).text() + ui.values[ 1 ] );
      }
    });
    $( "#price-amount-label" ).val( $("<div>").html(Settings.currency.char).text() + $( "#slider-price" ).slider( "values", 0 ) +
      " - " + $("<div>").html(Settings.currency.char).text() + $( "#slider-price" ).slider( "values", 1 ) );
});

$(function() {
    $( "#slider-storage" ).slider({
      range: true,
      min: 1,
      max: 256,
      values: [ 16, 64 ],
      slide: function( event, ui ) {
        $( "#storage-amount-label" ).val( "GB" + ui.values[ 0 ] + " - GB" + ui.values[ 1 ] );
      }
    });
    $( "#storage-amount-label" ).val( $( "#slider-storage" ).slider( "values", 0 ) +
      "GB - " + $( "#slider-storage" ).slider( "values", 1 ) + "GB");
});

$(function() {
    $( "#slider-ram" ).slider({
      range: true,
      min: 1,
      max: 16,
      values: [ 2, 4 ],
      slide: function( event, ui ) {
        $( "#ram-amount-label" ).val( ui.values[ 0 ] + "GB - " + ui.values[ 1 ] + "GB" );
      }
    });
    $( "#ram-amount-label" ).val( $( "#slider-ram" ).slider( "values", 0 ) +
      "GB - " + $( "#slider-ram" ).slider( "values", 1 ) + "GB" );
});

(function(b){b.support.touch="ontouchend" in document;if(!b.support.touch){return;}var c=b.ui.mouse.prototype,e=c._mouseInit,a;function d(g,h){if(g.originalEvent.touches.length>1){return;}g.preventDefault();var i=g.originalEvent.changedTouches[0],f=document.createEvent("MouseEvents");f.initMouseEvent(h,true,true,window,1,i.screenX,i.screenY,i.clientX,i.clientY,false,false,false,false,0,null);g.target.dispatchEvent(f);}c._touchStart=function(g){var f=this;if(a||!f._mouseCapture(g.originalEvent.changedTouches[0])){return;}a=true;f._touchMoved=false;d(g,"mouseover");d(g,"mousemove");d(g,"mousedown");};c._touchMove=function(f){if(!a){return;}this._touchMoved=true;d(f,"mousemove");};c._touchEnd=function(f){if(!a){return;}d(f,"mouseup");d(f,"mouseout");if(!this._touchMoved){d(f,"click");}a=false;};c._mouseInit=function(){var f=this;f.element.bind("touchstart",b.proxy(f,"_touchStart")).bind("touchmove",b.proxy(f,"_touchMove")).bind("touchend",b.proxy(f,"_touchEnd"));e.call(f);};})(jQuery);

app.initialize();