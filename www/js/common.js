/*
* VarMap = { key : callback, key2 : callback2, etc.}
*/

var Util = {
	appendModule: function(where, what, injectionMap, callback) {
		$.ajax({
		    url: what,
	        type: 'GET',
	        success: function(data){
	        	var html = $(data);
	        	if (injectionMap != 'undefined' && injectionMap != null) {
		        	for (var key in injectionMap) {
		        		var injectionCallback = injectionMap[key];
		        		if (key === 'self') {
		        			injectionCallback(html);
		        		}else {
		        			injectionCallback(html.find(key));
		        		}
		        	}
		        }
	    		$(where).append(html);
	    		if (callback != 'undefined' && callback != null) {
	    			callback();
	    		}
	        }
		});
	}
};

var Settings = {
	currency: {
		name: 'euro',
		char: '&euro;'
	},
	home: {
		url: 'index.html',
		nbr_item_loading: 6,
		first_items_load_nbr: 6
	},
	comparator: {
		url: 'comparator.html'
	},
	images: {
		format: '.png'
	}
};